# vim-polyglot for Pearl

A solid language pack for Vim.

## Details

- Plugin: https://github.com/sheerun/vim-polyglot
- Pearl: https://github.com/pearl-core/pearl
